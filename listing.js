const fs = require('fs')
const glob = require('glob')
const EOL = require('os').EOL
const config = require('./config.json')

glob(`${config.path}/${config.extension}`, {}, (err, files) => {
  if (err) {
    return console.error(err)
  }

  const a = files.reduce((acc, file) => {
    if (file.match(`${config.ignore}`)) {
      return acc
    }
    acc = acc + file.replace(`${path}`, '')
    acc = acc + EOL
    acc = acc + EOL
    acc = acc + fs.readFileSync(file).toString()
    acc = acc + EOL
    acc = acc + EOL
    acc = acc + EOL
    return acc
  }, '')
  fs.writeFileSync(config.out, a)
})
